<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(\App\User::count() == 0)
        {
            \App\User::create([
                'name'=>'Super Admin',
                'email'=>'superadmin@gmail.com',
                'password'=>bcrypt('123456'),
                'role'=>\App\User::superAdmin,
            ]);
        }
    }
}

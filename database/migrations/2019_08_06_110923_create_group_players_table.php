<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('player_group_id');
            $table->integer('player_id');
            $table->timestamps();

//            $table->foreign('player_group_id')->references('id')->on('player_groups')->onDelete('cascade');
//            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_players');
    }
}

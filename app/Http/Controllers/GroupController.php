<?php

namespace App\Http\Controllers;

use App\GroupPlayer;
use App\Player;
use App\PlayerGroup;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the Groups.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups=PlayerGroup::with('players','players.playerDetail')->get();
        $players=Player::get();
        return view('group.index',['groups'=>$groups,'players'=>$players]);
    }


    /**
     * Store a newly created Group
     *
     * @return \Illuminate\Http\Response
     */

    public function storeGroup(Request $request)
    {

        $this->validate($request,[
           'name'=>'required',
            'players'  => 'required|array|min:1',
        ]);

        $group=PlayerGroup::create([
            'name'=>$request->name
        ]);

        foreach ($request->players as $player)
        {
            GroupPlayer::create([
                'player_group_id'=>$group->id,
                'player_id'=>$player,
            ]);
        }

        return redirect(route('groups'))->with('success','Player Group added successfully.');
    }

    /**
     * Delete existing Group
     *
     * @return \Illuminate\Http\Response
     */
    public function removeGroup($id)
    {
        $group=PlayerGroup::find($id);
        $group->delete();
        return redirect(route('groups'))->with('success','Player Group deleted successfully.');
    }
}

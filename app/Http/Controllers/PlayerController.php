<?php

namespace App\Http\Controllers;

use App\Player;
use App\User;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    /**
     * Display a listing of the players.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $players= Player::get();
        return view('player.index',['players'=>$players]);

    }

    /**
     * Store a newly created player
     *
     * @return \Illuminate\Http\Response
     */
    public function storePlayer(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $player=new Player();
        $player->name=$request->name;
        // Check if photo file exist or not
        if ($request->hasFile('photo')) {
            $player->addMediaFromRequest('photo')->toMediaCollection('photo');
        }

        $player->save();

        return redirect(route('players'))->with('success','Player added successfully.');
    }

    public function removePlayer($id)
    {
        $player=Player::find($id);
        $player->delete();

        return redirect(route('players'))->with('success','Player deleted successfully.');
    }
}

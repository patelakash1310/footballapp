<?php

namespace App\Http\Controllers;


use App\PlayerGroup;
use App\Team;
use App\TeamGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    /**
     * Display a listing of the Teams.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $teams=Team::where('club_id',Auth::user()->club_id)->get();
        $groups=PlayerGroup::get();
        return view('team.index',['teams'=>$teams,'groups'=>$groups]);
    }

    /**
     * Store a newly created team
     *
     * @return \Illuminate\Http\Response
     */

    public function storeTeam(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'groups'  => 'required|array|min:1',
        ]);

        $team=Team::create([
           'club_id'=>Auth::user()->club_id,
            'name'=>$request->name,
        ]);


        foreach ($request->groups as $group)
        {
            TeamGroup::create([
                'team_id'=>$team->id,
                'group_id'=>$group,
            ]);
        }
        return redirect(route('teams'))->with('success','Teams added successfully.');
    }


    /**
     * Delete existing Group
     *
     * @return \Illuminate\Http\Response
     */
    public function removeTeam($id)
    {
        $team=Team::find($id);
        $team->delete();
        return redirect(route('teams'))->with('success','Team deleted successfully.');
    }


}

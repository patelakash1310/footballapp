<?php

namespace App\Http\Controllers;

use App\Club;
use App\Events\SendWelcomEmail;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::get();
        $clubs=Club::get();
        return view('user.index',['users'=>$users,'clubs'=>$clubs]);
    }


     /**
     * Store a newly created user
     *
     * @return \Illuminate\Http\Response
     */

    public function storeUser(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|unique:users,email',
        ]);

       $password = User::generatePassword();

        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->role=$request->user_type;
        if ($request->user_type == User::clubAdmin)
        {
            $user->club_id = $request->club;
        }
        // Check if profile picture file exist or not
        if ($request->hasFile('profile_picture')) {
            $user->addMediaFromRequest('profile_picture')->toMediaCollection('profile_picture');
        }

        $user->password=bcrypt($password);
        $user->save();

        // Send welcome email to new registered user
        event(new SendWelcomEmail($request->email,$request->name,$password));

        return redirect(route('users'))->with('success','User added successfully.');
    }

    /**
     * View user detail
     *
     * @return \Illuminate\Http\Response
     */

    public function viewUser($id)
    {
        $user=User::find($id);
        $club=Club::get();

        return view('user.index',['user'=>$user,'club'=>$club]);
    }

//    public function updateUser(Request $request)
//    {
//        dd($request);
//    }

    /**
     * Delete existing user
     *
     * @return \Illuminate\Http\Response
     */
    public function removeUser($id)
    {
       $user=User::find($id);
       $user->delete();

        return redirect(route('users'))->with('success','User deleted successfully.');
    }
}

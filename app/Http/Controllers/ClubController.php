<?php

namespace App\Http\Controllers;

use App\Club;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    public function index()
    {
        $clubs=Club::get();
        return view('club.index',['clubs'=>$clubs]);
    }

    public function storeClub(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $club=new Club();
        $club->name=$request->name;
        $club->description=$request->description;
        $club->save();

        return redirect(route('Clubs'))->with('success','Club Added Successfully.');
    }

    public function viewClub($id)
    {
        $club=Club::find($id);
        return view('club.view',['club'=>$club]);
    }

    public function updateClub(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $club=Club::find($request->id);
        $club->name=$request->name;
        $club->description=$request->description;
        $club->save();

        return redirect(route('viewClub',['id'=>$request->id]))->with('success','Club Updated Successfully.');
    }
}

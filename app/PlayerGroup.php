<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerGroup extends Model
{
    protected $fillable=['name'];


    public function players(){
        return $this->hasMany(GroupPlayer::class,'player_group_id','id');
    }
}

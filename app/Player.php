<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Player extends Model implements HasMedia
{
    use HasMediaTrait;
    use LogsActivity;

    protected $fillable=['name'];
}

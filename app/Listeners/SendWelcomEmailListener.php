<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
//        $user = User::find($event->user)->toArray();
        $user=['email'=>$event->email,'name'=>$event->name,'password'=>$event->password];
//        dd($user);
        Mail::send('emails.welcome', $user, function($message) {
            $message->to('akash@gmail.com');
            $message->subject('Event Testing');
        });
    }
}

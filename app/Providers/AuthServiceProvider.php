<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // define a super admin user role
        // returns true if user role is set to super admin
        Gate::define('isSuperAdmin', function($user) {
            return $user->role == 1;
        });

        // define a club admin user role
        // returns true if user role is set to club admin
        Gate::define('isClubAdmin', function($user) {
            return $user->role == 2;
        });
    }
}

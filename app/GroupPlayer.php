<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupPlayer extends Model
{
    protected $fillable=['player_group_id','player_id'];

    public function playerDetail(){
        return $this->hasOne(Player::class,'id','player_id');
    }
}

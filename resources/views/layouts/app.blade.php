<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    {{--    <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>--}}
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">




</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
                @auth
                    @can('isSuperAdmin', auth()->user())
                    <div class="row mb-5">
                        <div class="col-lg-8 offset-lg-2">
                            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                                {{--                <a class="navbar-brand" href="#">Navbar</a>--}}
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item @if(Request::is('home')) active @endif">
                                            <a class="nav-link" href="{{ route('home')}}">Home </a>
                                        </li>
                                        <li class="nav-item @if(Request::is('clubs')) active @endif">
                                            <a class="nav-link" href="{{ route('Clubs')}}">
                                                Clubs
                                            </a>
                                        </li>
                                        <li class="nav-item @if(Request::is('users')) active @endif">
                                            <a class="nav-link" href="{{ route('users')}}">
                                                Users
                                            </a>
                                        </li>

                                    </ul>

                                </div>
                            </nav>
                        </div>
                    </div>
                    @endcan

                        @can('isClubAdmin', auth()->user())
                    <div class="row mb-5">
                        <div class="col-lg-8 offset-lg-2">
                            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                                {{--                <a class="navbar-brand" href="#">Navbar</a>--}}
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item @if(Request::is('home')) active @endif">
                                            <a class="nav-link" href="{{ route('home')}}">Home </a>
                                        </li>

                                        <li class="nav-item @if(Request::is('players')) active @endif">
                                            <a class="nav-link" href="{{ route('players')}}">
                                                Players
                                            </a>
                                        </li>
                                        <li class="nav-item @if(Request::is('groups')) active @endif">
                                            <a class="nav-link" href="{{ route('groups')}}">
                                                Groups
                                            </a>
                                        </li>
                                        <li class="nav-item @if(Request::is('teams')) active @endif">
                                            <a class="nav-link" href="{{ route('teams')}}">
                                                Teams
                                            </a>
                                        </li>

                                    </ul>

                                </div>
                            </nav>
                        </div>
                    </div>
                    @endcan
                @endauth
            @yield('content')
        </main>
    </div>

    <div class="toast" role="alert alert-success" aria-live="assertive" aria-atomic="true" data-delay="3000" style="position: absolute; top: 1rem; right: 1rem;">
        <div class="toast-header">

            <strong class="mr-auto">Good Job</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            Hello, world! This is a toast message.
        </div>
    </div>

@yield('script')

</body>
</html>

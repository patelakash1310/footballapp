@extends('layouts.app')

@section('content')
<div class="container">


{{--    @can('isSuperAdmin')--}}
{{--        <li class="nav-item">--}}
{{--            Super Admin Account--}}
{{--        </li>--}}
{{--    @else--}}
{{--        <li class="nav-item">--}}
{{--            Club Admin Account--}}
{{--        </li>--}}
{{--    @endcan--}}
{{--    --}}

    <div class="row justify-content-center mb-5">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="{{route('storeUser')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="card-header">
                   Add New User
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label > Name</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label > Email</label>
                                <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                @if($errors->has('email'))
                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label > User Type</label>
                               <select class="form-control select-club" name="user_type" onchange="usertypeselected(this.value)">
                                <option value="0">Select User Type</option>
                                   <option value="1">Super Admin</option>
                                   <option value="2">Club Admin</option>
                               </select>
                                @if($errors->has('user_type'))
                                    <span class="text-danger">{{$errors->first('user_type')}}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4 selectClub " style="display: none;" >
                            <div class="form-group">
                                <label >Club</label>
                                <select class="form-control" name="club">
                                    <option value="0">Select Club</option>
                                    @foreach($clubs as $club)
                                        <option value="{{$club->id}}">{{$club->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('club'))
                                    <span class="text-danger">{{$errors->first('club')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label > Profile Picture</label>
                                <input type="file" class="form-control-file" name="profile_picture">
                                @if($errors->has('profile_picture'))
                                    <span class="text-danger">{{$errors->first('profile_picture')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                   <input type="submit" class="btn btn-primary" value="Submit">
                   <input type="reset" class="btn btn-danger" value="Clear">
                </div>
                </form>
            </div>
        </div>
    </div>
    @if(Session::has('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8"><h4 class="mt-1">List Of User</h4></div>
{{--                        <div class="col-md-4"> <a href="javascript:void(0)" class="btn btn-primary float-right" data-toggle="modal" data-target="#addClubModal">Add New Club</a></div>--}}
                    </div>

                </div>

                <div class="card-body">
                    <table id="clubsTable" class="table table-striped table-bordered">
                        <tr>
                            <th width="60px" class="text-center">Sr.No</th>
                            <th>Profile</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th width="170px" class="text-center">Action</th>
                        </tr>
                        @foreach($users as $key=>$user)
                        <tr>

                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">
                                @if($user->getMedia('profile_picture')->first())

                                    <img src="{{$user->getMedia('profile_picture')->first()->getUrl()}}" width="50px" height="50px">
                                @else
                                    <img src="{{asset('media/default-user.png')}}" width="50px" height="50px">

                                @endif
                            </td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if($user->role == \App\User::superAdmin)
                                    Super Admin
                                    @else
                                    Club Admin
                                @endif
                            </td>
                            <td class="text-center">
{{--                                <a href="{{route('viewUser',['id'=>$user->id])}}" class="btn btn-primary"><i class="fa fa-eye"></i> </a>--}}
                                <a href="{{route('removeUser',['id'=>$user->id])}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

    <script>
        $(document).ready(function() {
            usertypeselected($('.select-club').val());
        } );

        function usertypeselected(value) {
            
            if(value == 2)
            {
                $('.selectClub').css('display','block');
            }else{
                $('.selectClub').css('display','none');
            }

        }
    </script>
@endsection

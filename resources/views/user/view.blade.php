@extends('layouts.app')

@section('content')
<div class="container">


{{--    @can('isSuperAdmin')--}}
{{--        <li class="nav-item">--}}
{{--            Super Admin Account--}}
{{--        </li>--}}
{{--    @else--}}
{{--        <li class="nav-item">--}}
{{--            Club Admin Account--}}
{{--        </li>--}}
{{--    @endcan--}}
{{--    --}}

    <div class="row justify-content-center mb-5">
        <div class="col-md-12">
            <div class="card">

                <form method="post" action="{{route('updateUser')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="card-header">
                        Add New User
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label > Name</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label > Email</label>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label > User Type</label>
                                    <select class="form-control select-club" name="user_type" onchange="usertypeselected(this.value)">
                                        <option value="0">Select User Type</option>
                                        <option value="1">Super Admin</option>
                                        <option value="2">Club Admin</option>
                                    </select>
                                    @if($errors->has('user_type'))
                                        <span class="text-danger">{{$errors->first('user_type')}}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label > Profile Picture</label>
                                    <input type="file" class="form-control-file" name="profile_picture">
                                    @if($errors->has('profile_picture'))
                                        <span class="text-danger">{{$errors->first('profile_picture')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-danger" value="Clear">
                    </div>
                </form>


            </div>
        </div>
    </div>

</div>

@endsection

@section('script')

    <script>
        // $(document).ready(function() {
        //     $('#clubsTable table').DataTable();
        // } );
    </script>
@endsection

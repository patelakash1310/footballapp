@extends('layouts.app')

@section('content')
<div class="container">


{{--    @can('isSuperAdmin')--}}
{{--        <li class="nav-item">--}}
{{--            Super Admin Account--}}
{{--        </li>--}}
{{--    @else--}}
{{--        <li class="nav-item">--}}
{{--            Club Admin Account--}}
{{--        </li>--}}
{{--    @endcan--}}
{{--    --}}

    <div class="row justify-content-center mb-5">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="{{route('storeClub')}}">
                    {{csrf_field()}}
                <div class="card-header">
                   Add New Club
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label > Name</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label > Description</label>
                                <textarea class="form-control" name="description">{{old('description')}}</textarea>
                                @if($errors->has('description'))
                                    <span class="text-danger">{{$errors->first('description')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                   <input type="submit" class="btn btn-primary" value="Submit">
                   <input type="reset" class="btn btn-danger" value="Clear">
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8"><h4 class="mt-1">List Of Clubs</h4></div>
{{--                        <div class="col-md-4"> <a href="javascript:void(0)" class="btn btn-primary float-right" data-toggle="modal" data-target="#addClubModal">Add New Club</a></div>--}}
                    </div>

                </div>

                <div class="card-body">
                    <table id="clubsTable" class="table table-striped table-bordered">
                        <tr>
                            <th width="60px" class="text-center">Sr.No</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th width="170px" class="text-center">Action</th>
                        </tr>
                        @foreach($clubs as $key=>$club)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td>{{$club->name}}</td>
                            <td>{{$club->description}}</td>
                            <td class="text-center">
                                <a href="{{route('viewClub',['id'=>$club->id])}}" class="btn btn-primary"><i class="fa fa-eye"></i> </a>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

    <script>
        // $(document).ready(function() {
        //     $('#clubsTable table').DataTable();
        // } );
    </script>
@endsection

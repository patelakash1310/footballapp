@extends('layouts.app')

@section('content')
<div class="container">


{{--    @can('isSuperAdmin')--}}
{{--        <li class="nav-item">--}}
{{--            Super Admin Account--}}
{{--        </li>--}}
{{--    @else--}}
{{--        <li class="nav-item">--}}
{{--            Club Admin Account--}}
{{--        </li>--}}
{{--    @endcan--}}
{{--    --}}

    <div class="row justify-content-center mb-5">
        <div class="col-md-12">
            <div class="card">

                <form method="post" action="{{route('updateClub')}}">
                    {{csrf_field()}}
                    <div class="card-header">
                        Edit Club
                    </div>
                    <input type="hidden" name="id" value="{{$club->id}}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label > Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$club->name}}">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label > Description</label>
                                    <textarea class="form-control" name="description">{{$club->description}}</textarea>
                                    @if($errors->has('description'))
                                        <span class="text-danger">{{$errors->first('description')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <input type="submit" class="btn btn-primary" value="Update">
{{--                        <input type="reset" class="btn btn-danger" value="Clear">--}}
                    </div>
                </form>


            </div>
        </div>
    </div>

</div>

@endsection

@section('script')

    <script>
        // $(document).ready(function() {
        //     $('#clubsTable table').DataTable();
        // } );
    </script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">


{{--    @can('isSuperAdmin')--}}
{{--        <li class="nav-item">--}}
{{--            Super Admin Account--}}
{{--        </li>--}}
{{--    @else--}}
{{--        <li class="nav-item">--}}
{{--            Club Admin Account--}}
{{--        </li>--}}
{{--    @endcan--}}
{{--    --}}

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @can('isSuperAdmin', auth()->user())
                <div class="card-body">

                    Welcome Super Admin To Football Club
                </div>
                @endcan
                @can('isClubAdmin', auth()->user())
                    <div class="card-body">

                        Welcome {{auth()->user()->club->name}} Club Admin To Football Club
                    </div>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>
        // $(document).ready(function() {
        //     $('#clubsTable table').DataTable();
        // } );
    </script>
@endsection

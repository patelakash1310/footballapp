@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center mb-5">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="{{route('storePlayer')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="card-header">
                   Add New Player
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label > Name</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label > Photo</label>
                                <input type="file" class="form-control-file" name="photo">
                                @if($errors->has('photo'))
                                    <span class="text-danger">{{$errors->first('photo')}}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                   <input type="submit" class="btn btn-primary" value="Submit">
                   <input type="reset" class="btn btn-danger" value="Clear">
                </div>
                </form>
            </div>
        </div>
    </div>
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8"><h4 class="mt-1">List Of Players</h4></div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="clubsTable" class="table table-striped table-bordered">
                        <tr>
                            <th width="60px" class="text-center">Sr.No</th>
                            <th>Profile</th>
                            <th>Name</th>
                            <th width="170px" class="text-center">Action</th>
                        </tr>
                        @foreach($players as $key=>$user)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">
                                @if($user->getMedia('photo')->first())

                                    <img src="{{$user->getMedia('photo')->first()->getUrl()}}" width="50px" height="50px">
                                @else
                                    <img src="{{asset('media/default-user.png')}}" width="50px" height="50px">

                                @endif
                            </td>

                            <td>{{$user->name}}</td>

                            <td class="text-center">
                                <a href="{{route('removePlayer',['id'=>$user->id])}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>

                            </td>
                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

    <script>
        $(document).ready(function() {
            usertypeselected($('.select-club').val());
        } );

        function usertypeselected(value) {
            
            if(value == 2)
            {
                $('.selectClub').css('display','block');
            }else{
                $('.selectClub').css('display','none');
            }

        }
    </script>
@endsection

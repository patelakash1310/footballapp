<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
///************ Super Admin Routes ************/
//Route for clubs

    Route::get('clubs', 'ClubController@index')->name('Clubs');
    Route::post('add/club', 'ClubController@storeClub')->name('storeClub');
    Route::get('view/club/{id}', 'ClubController@viewClub')->name('viewClub');
    Route::post('update/club/', 'ClubController@updateClub')->name('updateClub');

//Route for Users
    Route::get('users', 'UserController@index')->name('users');
    Route::post('add/users', 'UserController@storeUser')->name('storeUser');
    Route::post('update/users', 'UserController@updateUser')->name('updateUser');
    Route::get('view/user/{id}', 'UserController@viewUser')->name('viewUser');
    Route::get('remove/user/{id}', 'UserController@removeUser')->name('removeUser');

///************ End Super Admin Routes ************/

///************ Club Admin Routes ************/

//Route for Player

    Route::get('players', 'PlayerController@index')->name('players');
    Route::post('add/player', 'PlayerController@storePlayer')->name('storePlayer');
    Route::get('remove/player/{id}', 'PlayerController@removePlayer')->name('removePlayer');


//Route for Group
    Route::get('groups', 'GroupController@index')->name('groups');
    Route::post('add/group', 'GroupController@storeGroup')->name('storeGroup');
    Route::get('remove/group/{id}', 'GroupController@removeGroup')->name('removeGroup');

//Route for Team
    Route::get('teams', 'TeamController@index')->name('teams');
    Route::post('add/team', 'TeamController@storeTeam')->name('storeTeam');
    Route::get('remove/team/{id}','TeamController@removeTeam')->name('removeTeam');

///************ End Club Admin Routes ************/

});
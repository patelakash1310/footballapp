


## Steps for Installation

- Clone repository
- Install composer using composer install command
- Make database and update database in .env
- Add mailtrap credential in .env 
- Run php artisan migrate
- Run php artisan db:seed
- Run php artisan serve

- you can login using below credential:
   
   Email : superadmin@gmail.com
   Password : 123456